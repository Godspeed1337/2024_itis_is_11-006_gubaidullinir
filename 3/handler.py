import os
import json
from collections import defaultdict


# Путь к папке с обработанными документами
processed_folder_path = '../2/processed/'


# Функция для чтения обработанного документа
def read_processed_document(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        return file.read().split()


# 1. Создание инвертированного индекса
def create_inverted_index(processed_folder_path):
    inverted_index = defaultdict(set)
    for processed_file in os.listdir(processed_folder_path):
        doc_id = int(processed_file.split("_")[1].split(".")[0])
        lemmas = read_processed_document(os.path.join(processed_folder_path, processed_file))
        for lemma in lemmas:
            inverted_index[lemma].add(doc_id)
    return dict(inverted_index)

# Сортировка индекса и сохранение в файл
inverted_index = create_inverted_index(processed_folder_path)
sorted_inverted_index = dict(sorted(inverted_index.items()))
sorted_inverted_index = {term: sorted(list(doc_ids)) for term, doc_ids in sorted_inverted_index.items()}

with open('inverted_index.json', 'w', encoding='utf-8') as f:
    json.dump(sorted_inverted_index, f, ensure_ascii=False, indent=4)


# 2. Реализация булева поиска
def boolean_search(query, inverted_index):
    op_and = 'И'
    op_or = 'ИЛИ'
    op_not = 'НЕ'

    # Разбор запроса
    # приводим к понятному для питона запроса
    query = query.replace('ИЛИ', '|').replace('И', '&').replace('!', 'НЕ ').replace('НЕ ', 'НЕ')

    words = set(query.split()) - {op_and, op_or}
    local_dict = {word: set(inverted_index.get(word, set())) for word in inverted_index}
    for word in words:
        # документы, в которых не встречается слово
        if word.startswith("НЕ"):
            local_dict[word] = set(range(1, 101)) - local_dict.get(word[2:], set())

    print(query)
    try:
        return eval(query, {}, local_dict)
    except NameError:
        return set()  # Возвращаем пустое множество, если термин не найден


# Пример использования булева поиска
query1 = "отзыв И удалённый ИЛИ удобный"
query2 = "отзыв ИЛИ удалённый ИЛИ удобный"
query3 = "отзыв И удалённый И удобный"
query4 = "отзыв И НЕ удалённый ИЛИ НЕ удобный"
query5 = "отзыв ИЛИ удалённый ИЛИ удобный"

with open('results.txt', "w", encoding="utf-8") as f:
    for query in (query1, query2, query3, query4, query5):
        search_results = boolean_search(query, sorted_inverted_index)
        f.write(f"{query}:\n {sorted(list(search_results))}\n")
