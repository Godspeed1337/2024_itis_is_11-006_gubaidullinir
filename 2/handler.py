import os
import nltk
import pymorphy2
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

# Необходимо для первого использования NLTK для загрузки списка стоп-слов
nltk.download('stopwords')
nltk.download('punkt')

folder_path = '../1/sites/'

# Инициализация анализатора pymorphy2 и получение списка стоп-слов
morph = pymorphy2.MorphAnalyzer()
russian_stopwords = stopwords.words("russian")


# Функция для обработки одного документа
def process_document(file_path):
    with open(file_path, 'r', encoding='utf-8') as file:
        text = file.read()

    # Токенизация
    tokens = word_tokenize(text, language="russian")

    # Лемматизация и фильтрация стоп-слов
    lemmas = [morph.parse(token)[0].normal_form for token in tokens if
              token not in russian_stopwords and token.isalpha()]

    return lemmas


# Обработка всех документов в папке
for i in range(1, 101):
    file_name = f"{i}.txt"
    file_path = os.path.join(folder_path, file_name)

    if os.path.exists(file_path):
        processed_text = process_document(file_path)

        # Сохранение обработанного документа
        new_file_path = os.path.join('./processed', f"processed_{i}.txt")
        with open(new_file_path, 'w', encoding='utf-8') as new_file:
            new_file.write(' '.join(processed_text))
    else:
        print(f"Файл {file_name} не найден.")

