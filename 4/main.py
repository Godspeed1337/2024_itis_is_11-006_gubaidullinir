import os
import json
import math

import numpy as np
import pandas as pd
from collections import defaultdict

# Путь к папке с обработанными документами
processed_folder_path = '../2/processed/'

# Загрузка инвертированного индекса
with open('../3/inverted_index.json', 'r', encoding='utf-8') as f:
    inverted_index = json.load(f)

# Количество документов в коллекции
total_docs = len(os.listdir(processed_folder_path))

# Подсчет встречаемости каждого термина в документах (tf) и подсчет документов, содержащих термин (df)
tf = defaultdict(lambda: defaultdict(float))  # Term frequency per document
df = defaultdict(int)  # Document frequency of terms

for doc_id in range(1, total_docs + 1):
    file_path = os.path.join(processed_folder_path, f'processed_{doc_id}.txt')
    with open(file_path, 'r', encoding='utf-8') as file:
        document_text = file.read()
        terms = document_text.split()

    # Считаем TF для каждого термина в документе
    for term in terms:
        tf[term][doc_id] += 1

    # Считаем DF для каждого термина
    for term in set(terms):
        df[term] += 1
        tf[term][doc_id] = tf[term][doc_id] / len(terms)

# tf-df
tf_df = pd.DataFrame.from_dict(tf).fillna(0).transpose()
tf_df = tf_df[sorted(tf_df.columns)]
tf_df = tf_df.round(6)
tf_df.sort_index(inplace=True)

# idf
idf_dict = {term: math.log10(total_docs / df) for term, df in df.items()}
idf_df = pd.DataFrame.from_dict(idf_dict, orient='index', columns=['IDF'])
idf_df.sort_index(inplace=True)

# tf-idf
idf_series = idf_df['IDF'].reindex(tf_df.index)
idf_diag_matrix = np.diag(idf_series)
tf_idf_matrix = np.dot(tf_df.transpose(), idf_diag_matrix)
tf_idf_df = pd.DataFrame(tf_idf_matrix.T, index=tf_df.index, columns=tf_df.columns)

excel_path = 'tf_idf_tables.xlsx'
with pd.ExcelWriter(excel_path) as writer:
    tf_df.to_excel(writer, sheet_name='TF')
    idf_df.to_excel(writer, sheet_name='IDF')
    tf_idf_df.to_excel(writer, sheet_name='TF-IDF')