import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity


tf_idf_df = pd.read_excel('../4/tf_idf_tables.xlsx', sheet_name='TF-IDF', index_col=0, keep_default_na=False)
idf_series = pd.read_excel('../4/tf_idf_tables.xlsx', sheet_name='IDF', index_col=0, keep_default_na=False)['IDF']


def compute_query_tf(query, tf_idf_df):
    term_freq = {term: 0 for term in tf_idf_df.index}
    terms = query.lower().split()

    for term in terms:
        if term in term_freq:
            term_freq[term] += 1

    tf_query = pd.DataFrame([term_freq])
    tf_query = tf_query.div(len(terms))

    return tf_query


def compute_query_tf_idf(query_tf, idf_series):
    tf_idf_query = query_tf.reindex(columns=idf_series.index, fill_value=0) * idf_series
    return tf_idf_query


def get_similarity_scores(tf_idf_query, tf_idf_df):
    cosine_similarities = cosine_similarity(tf_idf_query, tf_idf_df.T)
    similarity_scores = cosine_similarities[0]
    similarity_series = pd.Series(similarity_scores, index=tf_idf_df.columns)

    return similarity_series



query1 = "удалённый"
query2 = "удалённый python"
query3 = "удалённый python программирование"
queries = [query1, query2, query3]

with open('results.txt', 'w', encoding='UTF-8') as f:
    for query in queries:
        tf_query = compute_query_tf(query, tf_idf_df)
        tf_idf_query = compute_query_tf_idf(tf_query, idf_series)
        similarity_scores = get_similarity_scores(tf_idf_query, tf_idf_df)
        f.write(f"{query} :\n{similarity_scores.sort_values(ascending=False).head()}\n ----\n")
