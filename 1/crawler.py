import os

import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin

urls_set = {}


def recursion_crawl(url: str, index):
    try:
        response = requests.get(url, timeout=5)
    except:
        return
    if len(urls_set) >= 100:
        return
    urls = get_urls(response)
    new_index = index + 1
    # нужно, чтобы продолжить crawl, если не нашлись urls
    if len(urls) == 0:
        print('Текущий url:', url)
        print('не нашлись, перехожу к следующему', urls_set[new_index])
        recursion_crawl(urls_set[new_index], new_index)
    else:
        for link in urls:
            if len(urls_set) < 100:
                text = get_text_from_url(link)
                if len(text.split()) >= 1000:
                    print(link, len(text.split()))
                    urls_set[len(urls_set) + 1] = link
                    save_to_file(text, len(urls_set))
        # если страница изначальная оказалась меньше 1000 символов
        if url != urls_set[index]:
            new_index = index
        recursion_crawl(urls_set[new_index], new_index)


def get_urls(response: requests.models.Response):
    """
    Возвращает уникальный лист из urls, которых нет в словаре
    """
    soup = BeautifulSoup(response.text, "html.parser")
    all_a = soup.findAll('a')
    hrefs = set()
    urls_in_dict = set(urls_set.values())
    for a in all_a:
        href = a.get("href")

        href = urljoin(response.url, href)
        parsed_href = urlparse(href)

        href = parsed_href.scheme + "://" + parsed_href.netloc + parsed_href.path
        hrefs.add(href)
    uniq_urls = list(hrefs - urls_in_dict)
    # нужно выставить первым
    if response.url not in urls_in_dict and response.url not in uniq_urls:
        uniq_urls = [response.url] + uniq_urls
    return uniq_urls


def get_text_from_url(url):
    try:
        response = requests.get(url, timeout=5)
    except:
        return ''
    soup = BeautifulSoup(response.text, "html.parser")
    text = soup.get_text(' ', strip=True)
    return text


def save_to_file(text, index):
    with open(f"sites/{index}.txt", 'w', encoding="utf-8") as f:
        f.write(text)


def save_index_to_file():
    with open("index.txt", "w", encoding="utf-8") as f:
        for key, value in urls_set.items():
            f.write(f"{key}: {value}\n")


url = str(input("Enter URL "))
os.makedirs("./sites", exist_ok=True)
# url = 'https://habr.com/ru/articles/544828/'

recursion_crawl(url, 1)
save_index_to_file()
